﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebForms.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Web Forms</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Label ID="lblWebForms" runat="server" Text="WEB FORMS" Font-Bold="True"></asp:Label>

        </div>
        <div>
            <asp:CheckBoxList ID="CheckBDias" runat="server">
            <asp:ListItem Value="0">Segunda-Feira</asp:ListItem>
            <asp:ListItem Value="1">Terça-Feira</asp:ListItem>
            <asp:ListItem Value="2">Quarta-Feira</asp:ListItem>
            <asp:ListItem Value="4">Quinta-Feira</asp:ListItem>
            <asp:ListItem Value="5">Sexta-Feira</asp:ListItem>
            <asp:ListItem Value="6">Sábado</asp:ListItem>
            <asp:ListItem Value="7">Domingo</asp:ListItem>
            </asp:CheckBoxList>
            <br />
            <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar" BackColor="#66FF66" ForeColor="White" Height="32px" OnClick="Button1_Click" Width="116px" />
            
        </div>
        <br />   
        <div>
            <asp:Label ID="lblResultado" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
