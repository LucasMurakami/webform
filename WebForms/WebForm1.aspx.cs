﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            lblResultado.Text = "Dias escolhidos: <br />";
            foreach (ListItem item in CheckBDias.Items)
            {
                if (item.Selected)
                {
                    lblResultado.Text += item.Text + "<br />";
                }
            }
        }
    }
}